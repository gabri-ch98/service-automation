server {
    listen 85;
    listen [::]:85;
    #server_name micro-servicies.com www.micro-servicies.com;

    location / {
       
        proxy_pass http://172.16.1.2:8081;
       
        limit_except POST {
       
          proxy_pass http://172.16.1.2:8080;

        }
    }
}
